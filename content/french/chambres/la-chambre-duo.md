---
title: La chambre Duo
description: Chambre double standard
image: /images/77028391_264170187874289_6450614165001207808_o.jpg
image2: /images/doublelit.jpg
image3: /images/salledebain3.jpg
tarifnuit: '60'
tarifweekend: '60'
tarifsemaine: '60'
tarifvacances: '60'
superficie: '22'
nbPers: '2'
literie: 2 lits simples
---

