---
title: La chambre Blanche
description: Accessible aux Personnes à Mobilité Réduite
image: /images/74602896_264170117874296_8772920256306872320_o.jpg
image2: /images/salle-de-bain.jpg
image3: /images/chambre.jpg
tarifnuit: '60'
tarifweekend: '60'
tarifsemaine: '60'
tarifvacances: '60'
superficie: '20'
nbPers: '2'
literie: 1 grand lit double
---

