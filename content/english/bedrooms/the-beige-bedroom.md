---
title: The Beige bedroom
description: Soundproof double bedroom
image1: /images/lit4-2.jpg
image2: /images/salledebain4.jpg
image3: /images/lit4.jpg
tarifnuit: '60'
tarifweekend: '60'
tarifsemaine: '60'
tarifvacances: '60'
superficie: '28'
nbPers: '2'
literie: 1 very big double bed
---

