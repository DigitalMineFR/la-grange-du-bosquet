---
title: The Grey room
description: Double bedroom
image1: /images/75009065_264170454540929_6817307958653747200_o.jpg
image2: /images/lit2.jpg
image3: /images/salledebain2.jpg
tarifnuit: '60'
tarifweekend: '60'
tarifsemaine: '60'
tarifvacances: '60'
superficie: '25'
nbPers: '2'
literie: 1 big double bed
---

